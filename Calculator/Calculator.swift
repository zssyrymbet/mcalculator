//
//  ViewController.swift
//  Calculator
//
//  Created by Zarina Syrymbet on 9/22/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class Calculator {
    var firstNumber: Int = 0
    var secondNumber: Int = 0
    var result: Int = 0
    var labelResult: String = ""

    func saveNumberValue(resultLabel: String) -> String {
        firstNumber = Int(resultLabel) ?? 0
        labelResult = ""
        return labelResult
    }

    func onZeroPressed() -> String {
        labelResult = "0"
        labelResult = ""
        return labelResult
    }

    func onAddPressed(resultLabel: String) -> String{
        secondNumber = Int(resultLabel) ?? 0
        result = firstNumber + secondNumber
        labelResult = String(result)
        firstNumber = result
        return labelResult
    }
    
    func onSubtructPressed(resultLabel: String) -> String{
        secondNumber = Int(resultLabel) ?? 0
        result = firstNumber - secondNumber
        labelResult = String(result)
        firstNumber = result
        return labelResult
    }
    
    func onMultiplyPressed(resultLabel: String) -> String{
        secondNumber = Int(resultLabel) ?? 0
        result = firstNumber * secondNumber
        labelResult = String(result)
        firstNumber = result
        return labelResult
    }
    
    func onDividePressed(resultLabel: String) -> String{
        secondNumber = Int(resultLabel) ?? 0
           if(secondNumber == 0){
               labelResult = "Cannot divided by 0"
           }else{
               result = firstNumber / secondNumber
               labelResult = String(result)
               firstNumber = result
           }
        return labelResult
    }
    
    func clear() -> String{
        firstNumber = 0;
        secondNumber = 0
        result = 0
        labelResult = ""
        return labelResult
    }
}

