//
//  ViewController.swift
//  Calculator
//
//  Created by Zarina Syrymbet on 9/22/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//


import UIKit

class ViewController: UIViewController {
    
    var calculator = Calculator()
    var operation: Int = 0
    
    @IBOutlet weak var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func OnPressedActions(_ sender: UIButton) {
        
        if(sender.tag <= 10){
            resultLabel.text = resultLabel.text! + String(sender.tag)
        }else if(sender.tag == 11){
            resultLabel.text = calculator.clear()
        }else if(sender.tag == 12){
            switch (operation) {
                case 1:
                    resultLabel.text = calculator.onAddPressed(resultLabel: resultLabel.text!)
                case 2:
                    resultLabel.text = calculator.onSubtructPressed(resultLabel: resultLabel.text!)
                case 3:
                    resultLabel.text = calculator.onMultiplyPressed(resultLabel: resultLabel.text!)
                case 4:
                    resultLabel.text = calculator.onDividePressed(resultLabel: resultLabel.text!)
                default:
                    resultLabel.text = ""
            }
        }else{
            resultLabel.text = calculator.saveNumberValue(resultLabel: resultLabel.text!)
            switch (sender.tag) {
                case 13:
                    operation = 1
                case 14:
                    operation = 2
                case 15:
                    operation = 3
                case 16:
                    operation = 4
                default:
                    resultLabel.text = ""
            }
        }
    }
}



